$(window).on('resize', function(event) {
  $('iframe').each(function() {
    var iframe = $(this);
    var maxWidth = iframe.parent().width();

    var height;
    console.log('ah');

    var maxHeight = parseInt(iframe.css('max-height'));

    if (iframe.data('aspectRatio')) {
      height = maxWidth / parseFloat(iframe.data('aspectRatio'));

      if (height > maxHeight) {
        height = maxHeight;
        var width = height * parseFloat(iframe.data('aspectRatio'));
        iframe.css({'width': width});
      }
      else {
        iframe.css({'width': '100%'});
      }
      iframe.css({'height': height});
    }
  });
});

$(function() {
  $('iframe').each(function() {
    var iframe = $(this);

    window.addEventListener('message', function(event) {
      var message = event.data;

      if (event.source === iframe[0].contentWindow) {
        if (message.name === 'updateSize') {
           var height;

           if (message.data && message.data.aspectRatio) {
             // It gave us an aspect ratio, so we can resize width and height.
             height = iframe.width() / message.data.aspectRatio;
             iframe.data('aspectRatio', message.data.aspectRatio);

             var maxHeight = parseInt(iframe.css('max-height'));
             height = iframe.width() / parseFloat(iframe.data('aspectRatio'));

             if (height > maxHeight) {
               height = maxHeight;
               var width = height * parseFloat(iframe.data('aspectRatio'));
               iframe.css({'width': width});
             }
             else {
               iframe.css({'width': '100%'});
             }
           }
           else if (message.data && message.data.height) {
             // It gave us an explicit height, so we should just use that (within reason).
             height = message.data.height;
           }

           if (height) {
             iframe.css({'height': height});
           }
        }
      };
    });
  });
});
