---
layout: post
title: ISCA 2015 OCCAM Tutorial
categories: community events
---

Simulation forms a critical infrastructure for developing new computer architectures. The computer architecture community has developed many tools that encompass a rich and powerful set of capabilities. These tools are used in experiments to evaluate design trade-offs and to analyze and understand the implications to performance, energy, reliability, etc., of different choices. Despite the tremendous importance and variety of simulators available, the methods used for evaluation with the tools are often at odds with sound science and engineering.

We developed OCCAM with the basis of providing three supportive pillars: Infrastructure, Education, and Community. To that point, OCCAM is a distributed system for curating, distributing, and deploying simulators such that work can be independently repeated. When you instantiate an OCCAM system, you are able to use resources of any OCCAM node where you can discover and distribute simulators, benchmarks, and other research tools. The infrastructure allows faster and easier research paths for both students and dedicated researchers.

Our system is able to record and replay research work on any OCCAM instance, which allows both a unique style of collaboration and much-needed reproducibility and research extensibility. The OCCAM project also serves as a community-supported digital curator. Using our system, a developer can upload a simulator which can then be distributed within the OCCAM community and become immediately available. With just a few clicks, a researcher can have a simulator built and running on their system, or they can simply use any public OCCAM instance which already has the simulator installed through a web portal. We have already incorporated a collection of existing tools into the OCCAM system.

Our tutorial will demonstrate that OCCAM is a complete infrastructure for end-to-end research from hypothesis through experimentation and finally visualization. After the tutorial, participants will be able to immediately make use of and collaborate within the system for their own purposes. Participants will also have the knowledge to set up their own instances of OCCAM, which is freely and publicly available, where they can help curate tools, simulators, and research workloads within our distributed and collaborative system.

A full outline is available below. The tutorial will introduce the OCCAM system and tell 3 stories within 4 hours, including guided hands-on experience with a provided VM image:

* Development of research tools for use in repeatable experimentation within OCCAM
* Research using OCCAM for repeatability and collaboration
* Reviewing scientific artifacts through sharing via OCCAM

## Recommended Materials

To save time, please bring a laptop with VirtualBox already installed. This software is available for Windows, OS X, and Linux. We will provide a VirtualBox image that contains a pre-prepared OCCAM node for us to use and play with during the tutorial.

## Outline

### Introduction

* Discussion of motivations and philosophy
* State of computer science research and reproducibility
* The solution: three OCCAM pillars: infrastructure, community, and education

### OCCAM Infrastructure (Presentation)

* Distributed and federated system model
* Object import and curation
* Collaboration tools
* Repeatability

### OCCAM Basics Demonstration (Presentation)

* Introduce the workflow for a researcher
* How to import a simulator
* How to design and run an experiment
* How to graph the results

### Development with OCCAM (Guided, Hands-On)

* Introduction to building new tools
* Extending an existing simulator
* Importing and testing augmented Simulator
* Running and graphing experiments
* Exhibit curation of your extension

### Research with OCCAM (Guided, Hands-on)

* Introduction to research workflows
* Setting up privacy and collaboration
* Learning to generate work by sweeping configurations
* Using visualizations to explore results
* Using automated graphing and OCCAM's graph builder
* Publishing and sharing work and results

### Summary & Wrap-up

* Discussion of implications, future work, and question/answers.

## Schedule Information

* ISCA 2015: Portland, Oregon, USA
* Date: Sunday, June 14th, 2015
* Time: 1:30PM (PST) to 5:00pm
* Room: B119

## Tutorial Materials

Links to the worksets generated during the tutorial:

* DRAMSim2 examples: [simple](https://occam.cs.pitt.edu/worksets/workset-c765f51a-12b5-11e5-89d6-000af7451cc2/experiments/experiment-d0ee8c46-12b5-11e5-89d6-000af7451cc2) and [complex](https://occam.cs.pitt.edu/worksets/workset-c765f51a-12b5-11e5-89d6-000af7451cc2/experiments/experiment-3d2e85e0-12b7-11e5-89d6-000af7451cc2)
* HMMSim example: [2 cpu workflow](https://occam.cs.pitt.edu/worksets/workset-610569bc-12b6-11e5-89d6-000af7451cc2/experiments/experiment-6a4a6e78-12b6-11e5-89d6-000af7451cc2)

## Presenter Biographies

### Bruce Childers

is a Professor in the Computer Science Department at the University of Pittsburgh. His current research includes the development of memory systems that span embedded and data center  computing. Childers received a BS degree (CS, 1991) from the College of William and Mary and a PhD degree (CS, 2000) from the University of Virginia.

### Daniel Mosse

received a B.S. in Math from the University of Brasilia in 1986, and CS Ph.D. from the University of Maryland in 1993.  He joined the University of Pittsburgh in 1992, where he is currently Professor and Chair in the Computer Science Department.  Typically funded by NSF and DARPA, Dr. Mosse's projects combine theoretical results and actual implementations. His research interests span many specialties with a focus on saving energy in the last 10 years.

### Dave Wilkinson

received a B.S. in Computer Science in 2009 and an M.S. in 2014 from the University of Pittsburgh. A member of the W3C Social Web Working Group which develops standards for distributed social networks. Has founded rstatus, a well-known federated social network software, and is primarily interested in kernels, distributed systems and operating systems, particularly those with a collaborative or social focus.
