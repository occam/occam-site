---
index: 2
title: Objects
layout: doc_page
---

# Object Metadata

The `object.json` file contains various meta-data for an occam object, which can be a simulator, benchmark, trace, etc.
Excluding the source code location, much of this information is not relevant for object usage.
It is for properly citing the origins of the object and its authors.
This metadata can be automatically augmented by other OCCAM nodes to list mirrors such that OCCAM nodes can act as repositories for all source code and other artifacts associated with this object.

Here is an example `object.json` file for [DRAMSim2](https://wiki.umd.edu/DRAMSim2) which you can also find in our [code repository](https://bitbucket.org/occam/occam-simulator-dramsim2/src):

{% highlight javascript %}
{
  "type": "simulator",
  "name": "DRAMSim2",
  "id":   "simulator-12345678-1234-1234-1234-1234567890ab",

  "install": {
    "git": "git://github.com/dramninjasUMD/DRAMSim2.git"
  },

  "inputs": [
    {
      "type":  "trace",
      "group": "DRAMSim2"
    },
    {
      "type": "benchmark"
    }
  ],

  "outputs": [
    {
      "file":   "output.json",
      "schema": "output_schema.json",
      "type":   "application/json"
    }
  ],

  "configurations": [
    {
      "file":   "input.json",
      "schema": "input_schema.json",
      "name":   "DRAMSim2 Options",
      "type":   "application/json"
    }
  ],

  "build": {
    "using":    "base-66249f7e-9478-11e4-ac02-001fd05bb228",
    "script":   "build.sh",
    "language": "bash"
  },

  "run": {
    "script":   "launch.py",
    "language": "python",
    "version":  "3.3.0"
  },

  "website": "https://wiki.umd.edu/DRAMSim2",

  "description": "**DRAMSim** is a cycle accurate model of a DRAM memory controller, the DRAM
                  modules which comprise system storage, and the bus by which they communicate.
                  All major components in a modern memory system are modeled as their own
                  respective objects within the source, including: ranks, banks, command queue,
                  the memory controller, etc.\n\nThe overarching goal is to have a simulator
                  that is extremely small, portable, and accurate. The simulator core has a
                  well-defined interface which allows it to be CPU simulator agnostic and
                  should be easily modifiably to work with any simulator.  This core has no
                  external run time or build time dependencies and has been tested with g++ on
                  Linux as well as g++ on Cygwin on Windows.",

  "authors": ["Rosenfeld, P.", "Cooper-Balis, E.", "Jacob, B."],
  "organization": "University of Maryland, College Park",

  "license": "BSD",

  "tags": ["memory", "DRAM", "DDR2", "DDR3", "trace-based", "memory cards",
           "memory architecture", "DRAM architecture"],

  "citation": {
    "author": "Rosenfeld, P. and Cooper-Balis, E. and Jacob, B.",
    "journal": "Computer Architecture Letters",
    "title": "DRAMSim2: A Cycle Accurate Memory System Simulator",
    "year": "2011",
    "month": "jan.-june",
    "volume": "10",
    "number": "1",
    "pages": "16-19",
    "keywords": "DDR2/3 memory system model;DRAMSim2 simulation;DRAMSim2 timing;Verilog model;
                 cycle accurate memory system simulator;trace-based simulation;visualization tool;
                 DRAM chips;memory architecture;memory cards;",
    "doi": "10.1109/L-CA.2011.4",
    "ISSN": "1556-6056"
  }
}
{% endhighlight %}

## Authors

An array of free-form strings containing names of authors.
The actual formatting of the names is up to the author.

{% highlight javascript %}
"authors": ["Rosenfeld, P.", "Cooper-Balis, E.", "Jacob, B."]
{% endhighlight %}

## Build

This section of metadata describes the process of building the object once scripts have been retrieved and packages within the install section have been downloaded.

### Using

The using field specifies the base object to use when building this object. You can specify the name of an existing object, or, more likely, the name of an OCCAM base object.

{% highlight javascript %}
"build": {
  "using":    "ubuntu-base-2014-06",
  "script":   "build.sh",
  "language": "bash"
}
{% endhighlight %}

In the case above, the base image `ubuntu-base-2014-06` is used. The base name gives you an indication of the environment to expect. In this example, this is Ubuntu from June 2014. This environment is static. It will never change, and will be maintained. OCCAM will release new base images in this same style every 6 months which will upgrade the libraries and kernel.

### Script

This field specifies the script to run that will start the build. This script should be included along with the object.

### Language

{% highlight javascript %}
"build": {
  "using":    "ubuntu-base-2014-06",
  "script":   "build.py",
  "language": "python",
  "version":  "3.3.0"
}
{% endhighlight %}

This field specifies the language of the interpreter required to run the given build script. In the example above, it is python 3.3.0. The environment knows to install and build and use this particular version of python to run the script given.

### Version

This field specifies the version of the interpreter given by `language` that will run the given build script given by `script`. In the example listed under `language` above, it would run python version 3.3.0.

## Citation

A dictionary of citation parameters.
From this OCCAM can generate bibtex, etc formulations of citation meta-data to encourage simulator citation.

{% highlight javascript %}
"citation": {
  "author": "Rosenfeld, P. and Cooper-Balis, E. and Jacob, B.",
  "journal": "Computer Architecture Letters",
  "title": "DRAMSim2: A Cycle Accurate Memory System Simulator",
  "year": "2011",
  "month": "jan.-june",
  "volume": "10",
  "number": "1",
  "pages": "16-19",
  "keywords": "DDR2/3 memory system model;DRAMSim2 simulation;DRAMSim2 timing;Verilog model;
               cycle accurate memory system simulator;trace-based simulation;visualization tool;
               DRAM chips;memory architecture;memory cards;",
  "doi": "10.1109/L-CA.2011.4",
  "ISSN": "1556-6056"
}
{% endhighlight %}

## Configurations

This is an array of possible configurations. Each configuration consists of one or more structured configuration parameters. You can either specify them within the object description or specify a separate file to contain them. It is generally better to use an independent file to keep the object metadata file in `object.json` clean.

{% highlight javascript %}
"configurations": [
  {
    "file":   "input.json",
    "schema": "input_schema.json",
    "name":   "DRAMSim2 Options",
    "type":   "application/json"
  }
]
{% endhighlight %}

In the case above, we have one configuration called "DRAMSim2 Options" where the specification for what parameters exist are in the file `input_schema.json`. This file is specified as an [input schema](/input_schema.html). This object requests that the OCCAM runner produce a file called `input.json` with the configuration specified by the person running this object. It has an `application/json` type (the default.)

## Description

A long-form description of what the object does and what it is used for.
Markdown can be used to minimally style text.

{% highlight javascript %}
"description": "**DRAMSim** is a cycle accurate model of a DRAM memory controller, the DRAM
                modules which comprise system storage, and the bus by which they communicate.
                All major components in a modern memory system are modeled as their own
                respective objects within the source, including: ranks, banks, command queue,
                the memory controller, etc.\n\nThe overarching goal is to have a simulator
                that is extremely small, portable, and accurate. The simulator core has a
                well-defined interface which allows it to be CPU simulator agnostic and
                should be easily modifiably to work with any simulator.  This core has no
                external run time or build time dependencies and has been tested with g++ on
                Linux as well as g++ on Cygwin on Windows."
{% endhighlight %}

In the above, markdown rules are used to bold the term `DRAMSim` and two newlines near the middle
demarcate two paragraphs. For a basic rundown of markdown, [refer to this](https://daringfireball.net/projects/markdown/basics).

## File

This is a string containing a path that contains extra static data that is part of this object but not necessarily captured within the history of the object. This is used for situations where the object wraps some other type of content. For instance, it could wrap the standard output of a program represented by a text file. Or it could wrap a git repository by specifying a directory that contains the git metadata. These files are acknowledged as being part of the object and will be moved and stored along with it. For structured data, the `schema` attribute can also be used to attach a schema for the data.

## Inputs

This is an array of possible input objects that the specified object can use.

## Install

Within the install field is all information about the location of objects related to the retrieval of the object and all assets. This includes and mirrors. OCCAM nodes may add mirrors if such nodes discover mirrors or mirror the object itself.

### Git

The canonical URL to a public git repository that will provide the code for the object.
This will serve as the default place to acquire the newest source or source history.
The build scripts *may* be able to build different versions of the object, therefore
a git repository is preferred over a tar file for the purpose of reproducibility.

{% highlight javascript %}
"install": {
  "git": "git://github.com/dramninjasUMD/DRAMSim2.git"
}
{% endhighlight %}

### Mirrors

This object is optional and contains one or more non-canonical mirrors for object retrival. It contains any field normally allowed in an install block (git, hg, svn, tar) but with an array of strings for each mirror known.

OCCAM nodes may add their own entries to the mirrors section. OCCAM nodes should not consider the mirrors section to be part of the canonical object for the sake of equivalence.

{% highlight javascript %}
"install": {
  "git": "git://github.com/dramninjasUMD/DRAMSim2.git",
  "mirrors": {
    "git": ["https://occam.cs.pitt.edu/git/DRAMSim2.git",
            "https://occam.cs.cmu.edu/git/DRAMSim2.git"],
    "tar": ["https://occam.cs.pitt.edu/tar/DRAMSim2.git.tar.gz"]
  }
}
{% endhighlight %}

## License

A free-form string containing the software license the object uses.
If a object is dual-licensed, the most permissive license should be placed here.

{% highlight javascript %}
"license": "BSD"
{% endhighlight %}

## Name

The canonical name of the object which is how it will be displayed to people within OCCAM.

{% highlight javascript %}
"name": "DRAMSim2"
{% endhighlight %}

## Outputs

This is an array of possible output files that you can expect the object to produce.

## Organization

A free-form string containing the organization that develops or maintains the object.

{% highlight javascript %}
"organization": "University of Maryland, College Park"
{% endhighlight %}

## Tags

An array of tags used to better identify and curate objects.

{% highlight javascript %}
"tags": ["memory", "DRAM", "DDR2", "DDR3", "trace-based", "memory cards", "memory architecture", "DRAM architecture"]
{% endhighlight %}

## Type

The type of object that is being represented. Some standard object types are:

* simulator
* benchmark

## Website

The `URL` to the main website for the development and community of the object.

{% highlight javascript %}
"website": "https://wiki.umd.edu/DRAMSim2"
{% endhighlight %}
