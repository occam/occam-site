---
layout: doc_page
index: 1
title: Related Work
---

# Related Work

The topic of software archival and digital reproducibility invites a wide variety of interest. The best and most applicable solutions in this space will aggregate various technologies. As such, many projects and tools are relevant to the discussion even if archival or research is not their intended scope.

## Scientific Data Repositories

The traditional publishing of scientific papers has not deviated very far from the printed word. However, services have recently been developed that give researchers a better way of publishing code and supplemental digital media.

The open source movement has long motivated code hosting services. *GitHub* has over 10 million code repositories mostly consisting of publicly available software. {% cite 2015-GitHub %} {% cite 2013-GitHubStats %}
Similarly, *RunMyCode* is a centralized service where researchers can upload files related to their research. Contrary to the name, it does not provide any means of running the code. {% cite 2016-runmycode %} Similar sites include *ResearchCompendia* {% cite 2016-researchcompendia %} and *Zenodo*. {% cite 2016-zenodo %}

For specifically digital media, *Dataverse* is an open source web service that provides a means of collecting digital artifacts and research data. {% cite 2016-dataverse %} It can be installed as a self-hosted repository and ensures that data that is uploaded to any node is citable with a DOI. The web-service has built-in means of viewing common digital media. A similar digital media archive is *Archivematica*. {% cite 2016-archivematica %}

The *Open Science Framework* is a file hosting service designed for scientific research and collaboration. {% cite 2016-osf %} This repository allows active changes and focuses on both code and data. Researchers can upload sets of files and write documentation within a group as research work is completed. A collaborator can even link a repository on GitHub. When the research is published, a snapshot can be made to freeze a copy of the files at a particular time, including external resources, and a DOI provided to point to that snapshot.

Specialized research repositories also exist. *MyExperiment* is a centralized repository of research that specifically understands various scientific workflow file formats, such as Taverna 2. {% cite DeRoure2009561 %} Along with the software artifact data, the site renders the workflow such that people can explore the connections and inputs.

Overall, these digital archives can only preserve digital content statically. The growing use of code in research has driven a need for repositories that can also execute the code they archive.

## Executable Archives

With respect to repeatable execution, virtualization has been an attractive option. However, the trade-off of ease against virtual machine images on the order of gigabytes has been an obstacle.

The *Olive Archive* takes the approach of streaming the virtual machine image across the network where it is executed on a client machine on demand. {% cite 2015-Olive %}
Conversely, *bwFLA* approaches the problem by executing the virtual machine on commodity hardware and streaming the interactions through a remote desktop protocol. {% cite 2012-bwFLA %}
The *Internet Archive*, which has been better known for archiving static web pages across the Internet, takes a fully client-side approach. It uses the web browser to run relatively small emulators written in JavaScript. It has an emulation collection consisting of hundreds of DOS and Amiga software. {% cite 2016-internet-archive %}

*HUBzero* with its public *NanoHub* service integrates this idea of remote virtualization in the browser with the scientific process directly. {% cite 2016-hubzero %} {% cite 2012-nanohub %} This platform allows researchers the ability to create, use, and reuse scientific tools and publish those tools to their own site along with traditional content management. This created site allows others to run the tools to explore or verify results.

While these archives achieve the goal of allowing people to quickly tinker or play with artifacts, the use of specialized virtualization technologies is an overall inflexible choice. These all-or-nothing approaches do not easily adapt to newer techniques when the virtualization technology itself ceases to work when hardware and systems change.

## Executable Papers

As more research is being done with software tools and visualizations, there is a greater demand for a publication model that better reflects this interactivity. In 2011, Elsevier held the *Executable Paper Grand Challenge* on the topic of reproducible software in research publication. Of the 70 entries, there were nine finalists and three winners. The winner was *Collage* which allows researchers to write in a literate programming style incorporating runnable sections of code within your overall document. {% cite 2011-elsevier-executable-papers %} {% cite NOWAKOWSKI2011608 %}

Similarly, the *Jupyter* project gives the means to write interactive journals that combine prose and inline executable code. {% cite 2016-jupyter %} The output of the code can be a normal textual value or a graph, visualization, or other JavaScript interactive widget. Again, code and natural text are bundled and published together and can be viewed and computed through either a web-browser or command line tool.

*Codalab* is another interactive paper tool. {% cite 2016-codalab %} Unlike Jupyter, which assumes a self-contained model where one document is both code and data, Codalab separates the two. It gives researchers the ability to bundle many files together and keeping track of libraries and dependencies but still render them together. Therefore, it goes beyond simply publication to allow researchers the ability to run larger tasks in more practical, everyday settings.

## Containerization

Another approach to handle the complexity of a software environment which is seeing growing adoption is containerization. Unlike virtualization, containerization isolates applications but directly exposes them to the same hardware and kernel. Libraries and software can be installed in layers and shared across invocations where your software is run natively. Your programs get their own view of the filesystem and process tree. This has been mainly motivated by industry in maintaining web applications to provide a staging or development environment with the same operating system and libraries as your production server. In archives, this technology can provide the potential of virtualization without the inflexibility of large all-or-nothing images.

*Docker* is an open source project that makes use of Linux Containers and LXC, which are lower-level packages that isolate applications from the main system environment. {% cite 2014-docker %} {% cite 2015-Docker %} Researchers have built services on top of Docker. *SmartContainers* {% cite 2016-smartcontainers %} is a tool that wraps calls to docker to preserve their invocation and provenance. *SIMULOCEAN* is a web-based service where a research developer can upload applications and other researchers can use that pool of applications to dispatch docker containers to available machines. {% cite 2016-simulocean %} The researcher can upload input files and monitor the running docker containers.

Another containerization tool is *Singularity*, which, like Docker, uses Linux Containers as its basis. {% cite 2016-singularity %} It diverges from Docker in that it is specifically designed for HPC and running programs across multiple machines. It specifically targets MPI-based applications, for instance. Containerization is useful here when those machines may have different operating system environments. This is particularly true when other unaffiliated researchers wish to reproduce the work.

Like virtualization, when container technologies can no longer run, or the host kernel changes, the software may no longer function. This suggests that the best repeatable software tool is one that can adapt when archival technologies fail. Ultimately, one that uses containerization in the short-term when the benefits are applicable and later can fallback to more comprehensive solutions.

## Workflow Execution Environments

Most research is not a simple A-to-B affair. Scientific data involves taking a relatively small idea, exploring a space and producing a lot of information, and aggregating that back down to make a conclusive point. One method of organizing that process is building a workflow to abstract away many modular operations. Naively, this seems like the trivial flowchart, but the number of solutions implies a hidden complexity and need.

*Taverna 2* is an open source workflow management system designed to give scientists the means to automate computational tasks for experimentation. {% cite wolstencroft2013taverna %} The project consists of a desktop application to create workflows and a separate player application to submit workflows for remote execution. There is also an online component to create workflows within a web browser. Other desktop applications include *VisTrails* {% cite bavoil2005vistrails %} and *Kepler*. {% cite altintas2004kepler %}

The *Galaxy* project is a web-based workflow tool specifically for data-intensive biomedical research. {% cite 2016-galaxy %} The tool can either be used on any already running node or installed on your own machine. Researchers can build workflows using preexisting bio-science modules and visualization tools.

*Jenkins* is a continuous integration toolchain used primarily in industry application testing that has some support for workflows to build and run applications and their test suites. {% cite 2016-jenkins %} With Jenkins, you write a specialized script to tell the deployment tool what commands to run, if they can run in parallel, and how their data is piped. Other script-based workflow tools include Pegasus. {% cite deelman-ic-2016 %}

## Job Scheduling and Deployment

Experimentation often requires heavy computational resources and executable archives will have to accommodate this.
Fortunately, early computing was expensive, and over many decades, tools have been developed to handle intelligent deployment and scheduling of tasks.
There are far more than is reasonable to list here, but some that are widely used in scientific experimentation follow.

*HTCondor*, formerly Condor, is a classical fair-share scheduling system for managing a compute cluster. {% cite condor-hunter %}
At its core, it is a queue that multiple people can use to run scripts on any machine as computation is available.
It can be extended by separate tools such as *DAGman* {% cite 2016-dagman %} that can schedule tasks based on their relationships and dependencies which could be defined by something much like a workflow. Similar tools include Altair PBS {% cite 2016-pbs %} and OpenLava. {% cite 2016-openlava %}

Apache Mesos and related service Mesosphere provide a service that flattens a cluster of machines and dispatches Docker containers to run on any node with the needed resources. {% cite 2016-mesos %} {% cite 2016-mesosphere %} It is designed primarily for application deployment where it can automatically provision application services as load demands across available resources of your cluster or an external infrastructure such as Amazon Web Services (AWS). A similar service includes Google's Kubernetes. {% cite 2016-kubernetes %} Both are alluring to saturate computational resources with experimentation.

So far, these projects have assumed that you will provide the infrastructure and hardware. In contrast, *DataMill* is a service where institutions can allocate their hardware resources to the cause of heterogenous experimentation. {% cite 2016-datamill %} The goal with DataMill is to allow researchers who are testing work that is sensitive to hardware diversity the ability to run experiments on a variety of architectures and devices. You give one archive of your scripts, tell the service what hardware it can run on and what its resource requirements are, and it will deploy your scripts to machines that match those notes.

## Distributed Data Protocols

Scientific research often involves large datasets and reproducibility archives of computational research will likely need a way to move this data around to the machines where jobs and work are deployed. Distributed data has long been interested in the goal of multiple site replication to fuel availability and integrity of data.

*Git* is a protocol and associated command line tool to track versions of files as they are changed. {% cite 2015-git %} The protocol allows for clones of entire directory structures to be made from one server to another, while also allowing the viewing a single file at a particular point in time. Each change (revision) is referred to by a content-addressable hash. It is very commonly used by developers in both research and industry to actively maintain source code.

*Kademlia* is a distributed hash table that allows any client node to discover servers that advertise a particular key. {% cite 2002-Maymounkov %} The protocol includes a means of finding content based on these keys in a way that can sort by a metric such as their distance. Often, this is used to build peer-to-peer file networks where the hashes are content-addressed representations of files which are replicated and shared as they are demanded. In its use in BitTorrent, it was used on networks with at least 15 million servers. {% cite 2013-wang-mainline-dht %} Kademlia can be extended for more specific usecases. For example, *Coral* {% cite 2004-coral %} builds off of Kademlia for distributing static assets and pages on the web.

*IPFS* is essentially the obvious combination of Git and Kademlia, offering both versioning and a distributed and federated file store. {% cite 2016-ipfs %}
Like Git, files are deduplicated and assigned content-addressable hashes representing data at a particular point in time. Those hashes are searchable via a Kademlia+Coral-inspired DHT such that the many blocks that make up a single object can be retrieved from any set of machines which may store only some subset. The combination of versioning and deduplication makes the storage and use of larger datasets more reasonable for any long-term reproducibility effort. Also, the nature of a federated system means datasets and research software become widely available: simply link them.

## Repeatable Execution

As opposed to an all-encompassing virtual machine image, some tools have been developed around the idea of archiving just the software, its dependencies, and how to invoke it. The tools add metadata and logic to build an environment that can execute the application.

*Collective Knowledge* is a project that packages software artifacts with the larger goal of crowd-sourcing. {% cite 2016-collectiveknowledge %} A well-packaged software artifact has its inputs and outputs described via metadata. This package can then be replicated on any machine and execute while aggregating all of the results and failures globally. There are means of packaging the software such that if a researcher is thoughtful, the software can build and run consistently.

A similar project is *Umbrella* which also has a researcher manual package software and describe its environment, however Umbrella will decide how to run that package dynamically. {% cite Meng:2015:UPE:2755979.2755982 %} It will either run an artifact directly, use a virtual machine, or run within a container.

Since manual packaging has an element of human error, another approach is to automate the packaging process. *ReproZip* is a tool that when a researcher gives it some software to run, it will track the data and libraries that software uses and package them together. {% cite 2016-reprozip %} It uses software tracing functionality that allows ReproZip to be notified of every low-level call to the operating system so that it can see actions such as opening a file. This has inspired other more specialized tools such as *Light-weight Database Virtualization* (or LDV) which can also track and repeat database interactions across a network. {% cite pham2015ldv %}

## Specifications

The broad scope of archives of digital research require good standards for describing the work such that it can be queried and retrieved. Good metadata that reflects the relationships among a variety of different research artifacts will aid greatly in discoverability and citability.

To directly cite a digital work, the need for a unique identifier, much like an ISBN for books, became apparent. Currently, this is primarily solved using a Digital Object Identifier (or DOI) which is a non-for-profit service that can generate such an identifier for any digital object. The identifier is kept in a central database and resolves to a URL that yields the work. Non-profit services to assign DOIs have emerged such as *DataCite* which also includes the ability to attach further metadata to the DOI. {% cite 2016-datacite %}

With respect to packaged digital artifacts, the *BagIt* standard serves as a means of describing a hierarchical collection of files. {% cite I-D.kunze-bagit %} It requires a specific file layout and specifies content hashes for each file to ensure integrity. It works to describe any of digital media, software, and code.

A more comprehensive standard for software artifact metadata is Open Archives Initiative's *Object Reuse and Exchange* (or *OAI-ORE*) standard. {% cite 2008-oai-ore %} This builds on top of Linked Data (LD) and Resource Description Framework (RDF) standards serving to model the Semantic Web. Like BagIt, it describes an aggregate of resources or files. However, due to being built off of RDF or XML, the standard can be extended to add other standards such as W3C's *PROV-O* which could be used to capture the provenance of scientific workflows. {% cite 2013-w3c-provo %}

Another specification project is *Research Objects*, which is a reinvention of OAI-ORE with specific vocabulary for scientific artifacts and workflows. {% cite 2016-researchobjects %} As an RDF base, it can also be extended with such extensions as *wfprov* which also builds off of PROV-O to describe scientific workflows. {% cite 2016-researchobjects-specs %}

These standards overall help archives interoperate and help provide a consistent means of discovery of artifacts between otherwise divergent digital libraries.

## References

{% bibliography --cited %}
