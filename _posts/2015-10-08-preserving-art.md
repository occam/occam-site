---
layout: post
title:  "Preserving Art and Code with OCCAM"
date:   2015-10-08 01:10:48
time:   10 minutes
categories: technical art
---

<!-- hello -->

{% toc %}

In this article, we quickly discuss how we preserve digital artifacts, in this case nostalgic DOS games, in a way that will be future-proof. One of the problems that OCCAM solves is how exactly to describe a computational object in a way that future systems know how to run it. If you over-describe your process, it may become more difficult to run the application again 10 years from now as the tools you used to run it may themselves become deprecated by age.

## Motivation

![float|right|width=320|!](/images/preserving-art/dos.png) Perhaps our use of video games here makes the motivation somewhat trivial. However, the general motivation of preserving useful applications for future generations surpasses pure nostalgia. These artifacts might be tools that are used to view media in which case the digital form of the Betamax or even the VHS would be disastrous if lost. Even the metaphor seems more outdated than useful in reaching a younger audience.

Obviously one of our core goals is the preservation of scientific research. It goes beyond the VHS metaphor of even being able to read the results and data of a study, but also the archival of every tool and dataset used to produce that data. This is called provenance. Specifically, I call this computational provenance.

We can illustrate our system's ability to repeat work in a future-proof manner very clearly by using something obviously outdated. Here we will show that we can run these old DOS games on a modern system, but also show that the same process can determine how to run that same game on a hypothetical future system.

## One Must Fall 2097

![left|float|!](/images/preserving-art/omf.png)The game chosen here is Epic MegaGames' (now [Epic Games](http://epicgames.com)) 1994 cult hit *One Must Fall 2097*. This game about a future world ruled by imperial corporations settling disputes through industrial robots repurposed to fight is the perfect 20+ year old example of a fast-paced, archaic environment to reproduce. Also it is freeware, so hopefully no real-life corporate disputes either.

First, it is meant to be played on a DOS system. Computers these days are frankly far removed from the days of DOS. This is an environment where applications generally had full control over the system. The operating system simply gave control over to the application and it did all of the work. Typically they had their own drivers embedded into the code of the application or made limited use of so-called Resident Tasks where drivers were loaded to well-known locations of the system's memory and applications could choose whether or not to respect them or even use them. What a nightmare.

Archiving such applications is much like archiving and running an entire machine or kernel software. They antagonize archival and future repeatability by asking so much of the system's control. We cannot just allow an application full control over the machine. Even if we did, the ability to run DOS applications natively on hardware is difficult if not impossible. For these reasons, we can be reasonably justified in considering this 16-bit version of x86 a completely different architecture altogether. We will solve this problem generally.

Furthermore, some of its functionality of a DOS game is designed based on the CPU speed of its day. That means, the faster your machine is, the faster some of the responses in the game will be. Replicating the intended environment of the game such that it remains playable is a difficulty inherent in archival. Where some archivists will prefer, for perhaps this reason, replicating the hardware as part of provenance, we settle for only repeating hardware through software using techniques such as emulation or virtualization. Luckily, such emulators exist. Then the problem becomes: how do we archive the emulator in a way that preserves its future ability to run?

## The Case for Environments

![right|float|border|!](/images/preserving-art/omf-2097-environment.png) To this end, objects in OCCAM describe several things. We don't assume much about the object to begin with and limit the presumptions required by the system at large. Objects merely contain metadata that describes some human facing things such as a name and a type. The repeatability mechanism only focuses on the metadata that describes the process of building and running applications.

Here's the general idea. If I can build something, and then, assuming it is built, if I can run it, then it is useful. If I can recreate the environment that I used to build an application on a future system, then I can build it on that future system and thus run it as well. It is a simple premise that can expand to present a lot of complicated issues. However, the root of the problem of generalizing this archival process is capturing this sense of environment.

In the case of our DOS game, we need to capture the essence of this antagonistic environment. Programs of that era, much like programs of the modern era, and likely a concept that will continue for some time, were designed to run on many different systems that were mostly alike, but not exactly the same. I was not a developer at that time, but even today's development experience, particularly with the wide range of smartphones and tablet devices, would lead me to believe that this era of writing code for slightly different IBM compatible machines of that day was an awfully tedious experience. Luckily for us, this makes the archival problem, albeit counterintuitively, somewhat nicer.

We merely have to capture that essence: the concept that ties these different applications together. If we tag objects with that essence, "DOS" specifically in this case, and find a way of presenting that environment to the object, we can repeat the build process, reproduce the object, and play the object. For DOS, we have a couple of options. We can provide that environment by using a virtual machine to emulate a 16-bit architecture and then use a copy of DOS to more than faithfully reproduce that environment. In this case, though, I elected to use an emulator that emulates the operating system internally. Again, we don't need faithfulness. We only need the essence.

## Preserving Environments

So now we can have an object in our archive for this game. We then mark that game as requiring a "dos" environment on "x86" architecture. Now, at this point, OCCAM has no clue how to run this. That's a good thing. OCCAM should never just know how to run something. That wouldn't be very sustainable as that would make OCCAM too specific a tool. It isn't a tool that runs DOS games! What OCCAM specifically does know is **how to figure out** what it needs to run this object.

![border|!](/images/preserving-art/omf-2097-dosbox-environment.png)

The way this happens is that other software archived in the system can tell us what environments it provides. So, as we suggested, we will use DOSBox. This object will say, "hey, I provide the dos/x86 environment." When OCCAM sees that we want to run One Must Fall 2097, it looks for whatever object it can find that can match the environment. Now, it simply recurses. What does DOSBox need? A linux environment on x86-64? Well, that's our native environment, so we can just build and run that.

Now we are 10 years in the future running on some hypothetical machine. DOSBox doesn't run directly on this new architecture anymore. OCCAM doesn't mind. It knows that it needs to find a way to run DOSBox and, if it can, it knows it will be able to run One Must Fall. Let's assume there is some emulator for x86-64 that runs on this machine. Perfect. OCCAM can run this general x86-64 emulator, run linux on top, build DOSBox, run DOSBox with the game. Further in the future, it may expand this even further: an emulator for that hypothetical machine running the emulator for an x86-64 machine running... well, you can imagine.

![border|!](/images/preserving-art/omf-2097-dosbox-environment-future.png)

## The Unpredictable Future

Let's say at some point somebody writes a new DOS emulator specifically to run on this hypothetical future hardware, or simply ports DOSBox directly. She can add that object to OCCAM as a mapping between DOS and these future machines, and OCCAM will now find the shorter path. Over time, different methods and combinations of objects to run the same application can naturally occur. All of this is possible under the simple premise that replicating an environment can be done at any level of abstraction or emulation. Accuracy can even be measured by comparing the output from a successful run in the past to one that occurs on a brand-new path on some new hardware. In fact, this may just be a reasonable way of testing and allowing new architectures and operating system designs to exist without requiring the direct porting of existing applications.

At any rate, OCCAM preserves art, science, and the repeatability of code by archiving the means to replicate their environments. Science might produce new machines and new architectures and new systems, but there is absolutely no reason to allow science to cut itself off from its past, its work, nor its play.

![border|One Must Fall 2097 running within OCCAM's web portal](/images/preserving-art/omf-2097-occam.png)
