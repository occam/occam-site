---
layout: post
title:  "Provenance and Nostalgia"
date:   2015-11-16 04:54:33
time:   10 minutes
categories: technical
---

<!-- hello -->

{% toc %}

In this article, we will look at how OCCAM maintains provenance by looking at another nostalgic DOS game: DOOM. How can we maintain the recreation of objects from some source software? In this case, DOOM wasn't distributed as a game on its own, but rather it had an installer. We must archive the installer in such a way to maintain its relationship with a working copy of the game itself.

This is one of the stronger aspects of what we call **living archival** where archived objects can produce, and automatically archive, new material at any point in the future. Maybe even tens of years.
It is in this sense that OCCAM is built around ensuring that software works fast or even natively at first, and then gracefully falls back to stronger, albeit slower, virtualization methods when archived.
Playing back these stored versions of software, games, and tools is just a matter of learning how to build up the means of running some object as described in [Preserving Art and Code with OCCAM](/technical/2015/10/07/preserving-art.html).

## The Anatomy of an Installer

![width=500|left|float|Figure 1: Please Distribute??? OK!!!](/images/provenance-and-nostalgia/provenance_doom_install_video.png)

The DOOM Installer is a small, self-contained program that was generally
distributed among forums and bulletin boards. It is a program that when executed would install the game (decompress the game files contained within the installer) to a directory somewhere on the main drive.
Like most installers of its time, it would generally install the software off of the main drive (the C drive) simply in a single directory.
In this specific case, the DOOM Installer will just throw everything in a `DOOMS` directory by default unless you type in your own response.

We will assume that the person doing the installing just lets things run its course and installs the game in the default directory.
In our metadata for the installer, we will have a section not only for which command to run (`INSTALL.BAT`) and what type of environment to run it within (`dos/x86`), but we also specify that this object will create a `game` object when it finishes.

In the metadata below (some fields have been removed, such as authors and organization) you can see the minimum we need to run both the installer and eventually the game itself:

{% highlight javascript %}
{
  "id": "e53baa30-63c8-11e5-af85-001fd05bb228",

  "type": "installer",
  "name": "DOOM 1.9 Shareware Installer",

  "environment": "dos",
  "architecture": "x86",

  "install": [
    {
      "type": "application/zip",
      "source": "ftp://ftp.idsoftware.com/idstuff/doom/doom19s.zip",
      "to": "doom19s.zip",
      "name": "Doom 1.9 Shareware Archive",
      "unpack": true
    }
  ],

  "run": {
    "command": "INSTALL.BAT"
  },

  "outputs": [
    {
      "createIn": "DOOMS",

      "type": "game",
      "name": "DOOM 1.9 Shareware",

      "environment": "dos",
      "architecture": "x86",

      "run": {
        "command": "DOOM.EXE"
      }
    }
  ]
}
{% endhighlight %}

When the installer finishes, OCCAM will rake the outputs for that run.
The metadata tells it that it will create a `game` object within the "DOOMS" directory.
Thus whatever is in that path will also be committed to the object.
It will be given the type `game` and name `DOOM 1.9 Shareware` along with the remaining metadata.
Therefore it will also be a `dos/x86` object with the command `DOOM.EXE` to be executed.
This executable will be the one within that DOOMS path (which is now the root path of the new object)

## Provenance

![width=500|left|float|Figure 2: We created and archived DOOM as it was created by its installer. This record shows it was created from a VM.](/images/provenance-and-nostalgia/provenance_doom.png)

One of the important aspects of archival is the notion of *provenance*.
Knowledge of the origins of objects and the process they were created is **provenance**.
In this case, the provenance of the DOOM game is:

1. That it came from the DOOM Installer.
2. This installer was executed within a particular environment.
3. This installer's source was a zip file from an FTP site

We indeed preserve this information. In figure 2, the simple provenance is shown for the game.
This shows the exact circumstance the object was created.
In this case, it was created by a virtual machine running the DOOM Installer.
From here, you can view both the virtual machine and the metadata for the Installer.

When viewing the page for the DOOM Installer, you can then see the source for the zipfile.
Therefore, you know that the game was created as a direct result of the shareware distribution off of Id Software's FTP site.
When viewing the virtual machine (figure 3) we can then see all of the other software tools that aided us in our pursuit of nostalgia.

In this instance, the person that created the archive for the game needed to run the installer, which is itself a DOS program.
Therefore, OCCAM needed to create a virtual machine (in the way presented [in an aforementioned article](/technical/2015/10/07/preserving-art.html)) with
something that can recreate the environment (DOSBox) and some tools that allow them to interact with that environment (an X Server and VNC server.)

![width=500|float|right|The VM OCCAM created contains the DOOM installer and the various software it needs to run. Namely, DOSBox and an X Server.](/images/provenance-and-nostalgia/provenance_doom_install_vm.png)

The main reason we appreciate provenance is that it gives us some confidence about the quality of the archived tool.
Quality means a few things.
First, it can suggest how accurate this object is.
That is, were any tools used that could have interfered with the fidelity of what we produced?
Were the right tools and source materials used?
Secondly, it can give us an understanding of whether or not there was any human tampering with the object.
The more automated the process was to generate the object, the more convincing the produced content will be.
Furthermore, all the more reliable and consistent the process of reproduction will be.

We can look at this virtual machine and see that, outside of the obvious interaction required of the installer,
there is nothing out of the ordinary.
Only the minimum number of tools are used and they are fairly easy to understand.

All of this information is baked right into the generated object.
What follows is the metadata for the DOOM game as it is created by OCCAM after the DOOM Installer runs.
In fact, the entire provenance for the installer (including the zip file it sources) and the virtual machine (including all
metadata for every object used) will also be included verbosely here, but for the sake of the article it was been
truncated to just the minimum.

{% highlight javascript %}
{
  "type": "game",
  "name": "DOOM 1.9 Shareware",

  "id": "bbbbee56-cd6b-11e5-9984-001fd05bb228",

  "run": {
    "command": "DOOM.EXE"
  },

  "architecture": "x86",
  "environment": "dos",

  "generator": {
    "object": {
      "type": "installer",
      "name": "DOOM 1.9 Shareware Installer",
      "id": "e53baa30-63c8-11e5-af85-001fd05bb228",
      "revision": "5059c7852c4cae510b7f6e27a98dcc9e9a18ae1c"
    },
    "task": {
      "backend": "docker",
      "type": "occam/task",
      "name": "Virtual Machine to run DOOM 1.9 Shareware Installer with docker",
      "id": "37c96236-cd6b-11e5-9984-001fd05bb228",
      "revision": "f2889dc999863a2526c3fd742922f0100bb8fdae"
    }
  }
}
{% endhighlight %}

You can see the installer and the virtual machine are listed under `generator` and the metadata we need to run
the game is present. From this, we can link the installer and the VM and pull down that metadata and so on for whatever
generated them and create a tree that forms the entire provenance of every object used.

## Living Archival

![left|float|width=500|border|The Stanford Archive Vault, from 2000, [cynically discusses](http://link.springer.com/chapter/10.1007%2F3-540-45268-0_13) the need for archival tools to live alongside active production tools.](/images/provenance-and-nostalgia/sav_infomonitor.png)

All of this ties in to our goal of being a living archive.
Think about new things. New software and new tools.
This game runs in an emulator because the hardware and environment is was designed for has been deprecated by age.
Yet, we can still run it because there is software that can recreate the environment (DOSBox).

This is a proof of concept for newer tools.
Consider scientific research that uses a neuron simulator to digitally mimic the human brain.
This simulator is some code that generally assumes (as software is wont to do) some environment and architecture.
For modern tools, this might be a 64-bit x86 processor and some distribution of Linux.
If we run the archive tool on a machine that can yield this environment natively, then we can run the simulator natively.

This is important because simulators are intensive tasks.
They take a lot of hardware resources and would slow down too much to be useful for research if emulated or virtualized in any way.
However, we still must ensure that the task *can* be archived and used at a later date.
In this case, we will fall back to a technique very much like what is done here.
At some point we provide something that can provide a Linux/x86-64 environment and we can run what
right now runs natively on machines of the future.

For scientific research, this ensures that scientific accountability and progress can be made more explicit due to scientific reproducibility.
Science, in fact, dictates that reproducibility is required in the scientific method.
No result is a certainty, therefore each and every result must be capable of reexamination.
This is an effort to give that type of guarantee without stepping on too many toes.

Thus, we **use** and **reuse**. OCCAM is a tool that is useful now *and* later.
A living archive.

![E1M1 in all of its glory.](/images/provenance-and-nostalgia/provenance_doom_video.png)
