---
layout: about
title: "Federation"
index: 9
---

# Federation

## Object Discovery

The [Kademlia](https://en.wikipedia.org/wiki/Kademlia) protocol is used to implement a distributed hash table of nodes and objects throughout the system. Using this, objects can be thought of as *ubiquitous* and always present regardless of how you access the archive itself. If you install Occam on your personal machine, it will have access to all of the software in the world through other public Occam servers. You can start your own Occam server and curate the software you personally want to see in the future.

The technology is simple. Kademlia is a fancy way of looking up information by an index. If you have the identifier for an object, it enacts its sole purpose and finds out where the closest place you can get it from. This is called *discovery*, after which Occam can pull down the object and start asking other questions. One question the archive could ask itself is "How do I run this software?" On the surface, it seems simple, but there are a few possible directions. First, the software itself that you've pulled down can indicate directly what other software it needs in order to run. Occam will automatically pull down these supplementary objects. However, there are gaps in the information about how to get the program you want to run to work with your hardware. It is especially complicated by the fact that your hardware is likely newer than the computers of the time the original code was written!

In this case, Occam can start asking the server which stored the object for any help in running the object. It can ask that server "How would you run this for this environment or hardware?" And that Occam server can reply with a scenario that it thinks would work. It may respond with something it knows worked for somebody else, perhaps. Either way, your machine gets a cheatsheet of sorts that tell it what other software can help it get this old code to work. In the end, this is usually an emulator or virtual machine that can mimic old hardware and trick the code into thinking it is running on the real thing. Over time, what is interesting, is that the archive can learn new ways of running old things.

## Content Delivery

IPFS is a distributed file system that builds on top of Kademlia's discovery to provide a means of breaking large things into small things and distribute those to your personal machine from various locations. The goal with such a strategy is to preserve the potential of any one server by having multiple machines do a fraction of the work. This also encourages *replication* where content and code are cloned and thus are harder to accidentally destroy. The universe-level goal of this is to better preserve information and software near-permenantly.

### Issues

* Archive Poisoning - Overwriting information in the DHT with bogus information.
* UUID Collision - This is unintentional poisoning. Not a particular issue because objects are uniquely identified by their uuid first but also their type and publication date when available.
