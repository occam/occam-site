---
layout: about
title: Overview
categories: about
index: 0
---

# Overview

*OCCAM* stands for the **Open Curation of Computational Apparatus and Method**.
That is a fancy way of saying it is motivated by the idea that digital artifacts
should be preserved alongside the code and processes that were used to create
them.

Ultimately we want to give anyone the ability to create an archive of
software, science, simulations, visualizations, digital art and media,
interactive media, and more.
The system, at the high level, allows software objects to be added to the
archive and executed at any time while preserving the means to run the programs
at any future time.

The project serves each of the scientific, visualization, and art communities.
As such, the goal is to allow the system to grow and adapt to any one
community's needs. If the archive has a particular type of document, you can
tell the system what software could be used to open or play that document. It
will then run the viewing software while passing along the document as input.
The viewer software can be a javascript widget, in which case one can view the
document directly in a browser as a client-side application.

It is this flexibility that allows Occam to stand out and gives it the ability
to make stronger claims for the preservation of both software and digital media.

## Features

* Archives Source Code
* Stores External Resources [[Learn More](/resources)]
* Archive Extensibility [[Learn More](/viewers)]
* Repeatable Builds
* Reproducible Runs
* Workflows and Composition [[Learn More](/workflows)]
* Self-Hosted and Self-Curated

<div class='container'>
  <iframe src="https://occam.software/objects/bd4e25a0-5389-11e6-8a01-f23c910a26c8?embed&view=minimal"></iframe>
</div>
<div class='caption'>
  Here we have <a href="https://en.wikipedia.org/wiki/Commander_Keen">Commander Keen</a>, a DOS game running as a javascript component from within the archive. It is absolutely essential that we preserve both software and the ability to run that software. <a href="https://occam.software/objects/bd4e25a0-5389-11e6-8a01-f23c910a26c8">View on Occam</a>
</div>

## Philosophy

**Occam**'s mission is to enable all uses of computation and in doing so ensure that computation is archived and preserved for years to come. Emphasizing scientific uses of computers and code, Occam wants to push new modes of publication and verification of scientific work, particularly in the digital space. Design decisions must never get in the way of actively using or exploring existing work. The archive must not bias particular types of artifacts or organizations and allow any person to curate their own interests. To further that point, objects and code must not rely on being in one particular location and work across networks. To acheive these goals, the archive system itself must be extensible and learn how to execute and view various digital content as these artifacts are discovered and curated.
