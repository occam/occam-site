---
layout: about
title: Resources
categories: about
index: 3
---

# Resources

External resources are often pulled in by scripts so that programs can make use of packages and code that exist separate to itself or are hosted on some specific service. For instance, [zip files](https://en.wikipedia.org/wiki/Zip_(file_format)) are commonly used to hold together a set of files and compress them to save disk space. This is a convenient way of distributing software and has been for many years. Another more modern case is the distribution of source code. Programmers are making good use of version control, which is a technique where the changes you make to code are tracked instead of simply overwritten. Such version control tools such as git or mercurial have specific means of distribution and dedicated services such as [github](https://github.com) or [bitbucket](https://bitbucket.org).

![width=500|Github](/images/resources.svg)

When you build a software archive, you have to handle these external resources in a way that doesn't lose their provenance. That is, you want to know that a particular file or code repository was originally hosted at a particular site. In Occam, we can track external resources and archive them as they are used. The goal of our implementation is to allow the resource to be pulled from that external resource but not require that for repeatability. That is, we store the resource along with both the source URL of that resource and a tag that identifies it in our archive. When you need that resource again, you pull it from the archive according to the tag. If an Occam instance needs the resource but does not have it, it can pull it from the original source or from an Occam archive that does have it stored.
