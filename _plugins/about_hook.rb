Jekyll::Hooks.register :site, :post_read do |site|
  # Read about pages
  pages = site.pages.select do |page|
    page.path.start_with?("about/")
  end.map do |page|
    page.data.update({
      "url" => page.url,
    })
  end.sort do |a, b|
    a["index"] <=> b["index"]
  end

  site.config['about_pages'] = pages

  # Read docs pages
  pages = site.pages.select do |page|
    page.path.start_with?("docs/")
  end.map do |page|
    page.data.update({
      "url" => page.url,
    })
  end.sort do |a, b|
    a["index"] <=> b["index"]
  end

  site.config['doc_pages'] = pages
end
