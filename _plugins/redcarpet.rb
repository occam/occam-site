# This file contains overrides for markdown generation.
#
# Redcarpet is the original markdown parser and generator. It contains a class called
# Redcarpet::Render::HTML which is used to generate HTML from markdown. We add a few
# more useful features here.

require 'redcarpet'

module Jekyll
  class Node
    attr_accessor :child
    attr_accessor :sibling
    attr_accessor :parent
    attr_accessor :text
    attr_accessor :url
    attr_accessor :sibling_index

    def initialize(text, parent=nil, sibling=nil, child=nil, url=nil)
      @parent = parent
      @text = text
      @sibling = sibling
      @child = child
      @url = url
      @sibling_index = 1
    end

    def level
      level = 1
      current = @parent
      until current.nil? do
        current = current.parent
        level = level + 1
      end
      level
    end

    def slug
      if self.parent.parent
        "#{self.parent.slug}-#{self.sibling_index}"
      else
        "#{self.sibling_index}"
      end
    end
  end
end

module Redcarpet
  module Render
    class HTMLOutline < HTML
      attr_accessor :outline
      attr_accessor :title

      def reset
        @outline = Jekyll::Node.new(:root)
        @last    = @outline

        @outline = Jekyll::Node.new(:root)
        @last    = @outline
        @title   = ""
        @image_count = 0
      end

      def initialize(*args)
        reset
        super *args
      end

      # Headers (h1, h2, etc) to contain slug tags
      def header(text, header_level, classes=[], *args)
        text.strip!
        if text.start_with?('!step')
          text = text[5..-1]
          classes = classes.dup()
          classes << "step"
        end

        if header_level > 1
          new_node = Jekyll::Node.new(text)
          if header_level == @last.level
            new_node.parent = @last.parent
            @last.sibling = new_node
            new_node.sibling_index = @last.sibling_index + 1
          elsif header_level > @last.level
            new_node.parent = @last
            @last.child = new_node
          elsif header_level < @last.level
            new_node.parent = @last.parent.parent
            @last.parent.sibling = new_node
            new_node.sibling_index = @last.parent.sibling_index + 1
          end
          @last = new_node

          if not classes.empty?
            classes = " class='" + classes.join(' ') + "'"
          else
            classes = ""
          end

          "<h#{header_level} id='section-#{new_node.slug}'#{classes}>#{text}</h#{header_level}>"
        elsif header_level == 1
          @title = text
          "<h#{header_level} class='big' id='top'>#{text}</h#{header_level}>"
        end
      end

      def link(url, title, content)
        "<a href='#{url}' title='#{title}'>#{content}</a>"
      end
    end

    class HTML
      # Fix issue with dashes inside codespans.
      def codespan(code)
        "<code>#{CGI::escapeHTML(code).gsub(/\-/, "&#8209;")}</code>"
      end

      # Allow image captions, borders, and youtube embeds.
      def image(link, title, alt_text)
        unless link.match /^http|^\//
          link = "/images/#{@slug}/#{link}"
        end

        options = alt_text.match(/^(.*)\|/)
        options = options[1] if options

        alt_text.gsub!(/^.*\|/, "")

        styles      = ""
        img_styles  = ""
        classes     = "image"
        img_classes = ""

        if options
          options.split('|').each do |option|
            value = ""
            if option.index('=')
              option, value = option.split('=')
            end
            case option
            when "class"
              img_classes << value
            when "border"
              classes << " border"
            when "right"
              classes << " right"
            when "left"
              classes << " left"
            when "clear"
              styles << "clear: both;"
            when "float"
              classes << " float"
            when "width"
              unless value.end_with?("px")
                value = value + "px"
              end
              img_styles << "width: #{value};"
            when "fullwidth"
              classes << " fullwidth"
            end
          end
        end

        caption = ""
        caption = alt_text unless alt_text.start_with? "!"
        alt_text.gsub!(/^\!/, "")

        caption = Redcarpet::Markdown.new(self.class.new()).render(caption)
        alt_text = Nokogiri::HTML(alt_text).xpath("//text()").remove

        image_index = @image_count
        @image_count += 1
        img_source = "<a href='#{link}'><img src='#{link}' class='#{img_classes}' style='#{img_styles}' title='#{title}' alt='#{alt_text}' /></a>"
        img_source = "<a href='#{link}' data-lightbox='image-#{image_index}'><img src='#{link}' class='#{img_classes}' style='#{img_styles}' title='#{title}' alt='#{alt_text}' /></a>"

        if link.match "http[s]?://(www.)?youtube.com"
          # embed the youtube link
          youtube_hash = link.match("youtube.com/.*=(.*)$")[1]
          img_source = "<div class='youtube'><div class='youtube_fixture'><img src='/images/youtube_placeholder.png' /><iframe class='youtube_frame' src='http://www.youtube.com/embed/#{youtube_hash}'></iframe></div></div>"
        end

        caption = "<br /><div class='caption' style='#{img_styles}'>#{caption}</div>" unless caption == ""
        "</p><div style='#{styles}' class='#{classes}'>#{img_source}#{caption}</div><p>"
      end
    end
  end
end

class Jekyll::Converters::Markdown::CustomRedcarpet
  def initialize(config)
    @config = config
  end

  def extensions
    Hash[ *@config['redcarpet']['extensions'].map {|e| [e.to_sym, true] }.flatten ]
  end

  def markdown
    @markdown ||= ::Redcarpet::Markdown.new(Redcarpet::Render::HTMLOutline.new(self.extensions), self.extensions)
  end

  def convert(content)
    self.markdown.renderer.reset
    self.markdown.render(content) + "<div class='clear'></div>"
  end
end
