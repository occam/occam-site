module Jekyll
  class Scholar
    class Parseurl < BibTeX::Filter
      def apply(value)
        value.to_s.gsub(/\\url(\{(?:[^{}]|\g<1>)*\})/) do
          "<a href=\"#{$1[1..-2]}\">#{$1}</a>"
        end
      end
    end

    class BibliographyTag < Liquid::Tag
      alias_method :old_render_items, :render_items

      def render_items(items)
        ret = old_render_items(items)
        ret.to_s.gsub(/(span[^>]+?>)\[\d+?\]/) do
          "#{$1}"
        end
      end
    end
  end
end
