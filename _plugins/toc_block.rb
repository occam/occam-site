module Jekyll
  class TOCBlock < Liquid::Tag
    def initialize(tag_name, text, tokens)
      @markdown = ::Redcarpet::Markdown.new(Redcarpet::Render::HTMLOutline.new({}), {})
    end

    def render(context)
      content = super

      page_content = context.registers[:page]["content"].gsub(/\{%\s*step\s*%\}(.+?)\{%\s*endstep\s*%\}/, '### \1')

      @markdown.render(page_content)

      def parseSection(node)
        return "" if node.nil?

        ret = ""
        until node.nil? do
          ret << "<li><a href='#section-#{node.slug}'>#{node.text}</a>#{parseSection(node.child)}</li>"
          node = node.sibling
        end

        "<ul>#{ret}</ul>"
      end

      root = @markdown.renderer.outline

      "<nav class='toc'>#{parseSection(root.child)}</nav>"
    end
  end
end

Liquid::Template.register_tag('toc', Jekyll::TOCBlock)
