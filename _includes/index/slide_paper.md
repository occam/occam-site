## Interactive Papers

![width=400|border|!Shows an interactive graph with adjoining text which is being edited.](/images/slides/paper_configure.png)

**OCCAM** allows a person to write interactive *pages* where they can describe
the work and results. On this page, the author can embed any interactive widget
and configure that widget like any other object.

These widgets can take data generated from any experiment or simulator. Furthermore,
the widget tools can pull out a subset of that data on demand which makes it great for
not only plotting, but visualization. As always, provenance is recorded. The data, when
used as input to the widget, is recorded within the paper with its exact revision and id.

Embed the paper on your own personal website and go straight from the paper, the graph,
to the data and the experiment. Then just press run to see the results again.
