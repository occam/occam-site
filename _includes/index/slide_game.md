## Video Game Preservation

![width=400|border|!Shows an old image viewed by an old DOS program running in the browser.](/images/slides/vnc-lemmings.png)

**OCCAM** can preserve [many forms of digital art](http://deadreckoning.no-ip.org:8003/technical/2015/10/07/preserving-art.html). The most obvious case are video games via emulation. OCCAM doesn't just pre-package virtual machines to run games or other art. Instead, it takes a more flexible approach.

Based on the metadata, OCCAM decides how to build a virtual machine to run a piece of software on the fly. In this case, Lemmings is simply a package with an EXE that says it needs to run in DOS. A VM builder figures out that it can run this using a combination of Linux, DOSBox, and the game files and builds the VM for that goal. In the future, the VM builder may choose a different approach when computers change underneath meaning the archive can adjust and adapt as time passes.
