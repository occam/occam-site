## Document Preservation

![width=500|border|!Shows an old image viewed by an old DOS program running in the browser.](/images/slides/viewer_pcx_marbles.png)

**OCCAM** can run old GUI programs within the browser. These programs can be associated with certain types of objects as *viewers*. When properly associated, these programs can be used to open those files directly on that object's page. For instance in the image above, an old PCX file can be opened with a DOS program PICTVIEW. You can see the odd colors because the program is only 16 color, thus you can revisit the limitations and see exactly what others saw during that time.


