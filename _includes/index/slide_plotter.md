## Interactive Widgets

![width=500|!Plot.ly widget showing data from an experiment.](/images/slides/plotter_simple_dramsim2.png)

**OCCAM** can embed any javascript tool and archive it like any other object. You can use these tools to create plots and graphs of experiment data. Other javascript widgets serve as exploration tools and visualizations. You can always create your own widget from scratch or fork an existing one. When a graph is produced, the ability to replicate the data is preserved through the **provenance** stored that links the experiment from the graph itself.
