---
layout: post
title: Infrastructure
categories: infrastructure overview
---

OCCAM consists of several infrastructure components, including simulators, benchmark programs, experiments, and other evaluation artifacts.

## Simulators

Computer architecture researchers must choose a simulator to conduct their research on. Due to the sheer number of simulators, it can be difficult to find an appropriate simulator, potentially forcing researchers to "reinvent the wheel" and develop their own. The following table shows a snapshot of 31 different simulators and their capabilities:

![width=900|!](simulator-table-comparison.png)

As seen in the table above, it can be difficult for researchers to find appropriate simulators. There is no central repository that lists simulators and their features, and researchers are forced to scour the Internet and published papers looking for simulators. OCCAM aims to make it easy for researchers to find, use and, when appropriate, share newly developed simulators.

## Benchmarks

When comparing architectural techniques, it is necessary for researchers to rigorously evaluate a wide variety of representative workloads. OCCAM will provide centralized access to benchmarks and benchmark inputs, to allow researchers to more quickly and easily show that their techniques improve the state-of-the-art. Binary snapshots ensure that comparisons are made across identical workloads and that performance improvements are not due to, for example, different compiler flags.
